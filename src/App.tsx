import { Route, Router, Routes } from 'react-router';
import './App.css';
import './styles/sb-admin-2.min.css';
import { BrowserRouter } from 'react-router-dom';
import {Admin} from './pages/Admin';
import {Error} from './pages/Errors/Error';
import { PrivateRouter } from './components/PrivateRouter';
import { Login } from './pages/Account/Login';

function App() {

  return (
    <div className='App' id='wrapper'>
      <BrowserRouter>
        <Routes>
          <Route path='/Home' element={<Admin/>} errorElement={<Error/>}/>
          <PrivateRouter>
            <Login/>
          </PrivateRouter>
          <Route path='*' element={<Error/>} errorElement={<Error/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
