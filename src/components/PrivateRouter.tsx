import React from 'react'
import { Route, RouteProps } from 'react-router-dom'
import { Login } from '../pages/Account/Login/Login'

export const PrivateRouter = ({children, ...rest}: RouteProps) : JSX.Element => {
  return (
    <Route {...rest} render={() => (false ? children: <Login/>)}></Route>
  )
}
